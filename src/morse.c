#include "morse.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* morseCodes[] = {
        ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
        "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."
    };
const char letters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


void text_to_morse(const char* text){
    
    printf("\n");

    int text_size = 0;
    while(text[text_size] != '\0'){
        text_size++;
    }
    
    
    
    for(int i = 0; i < text_size; i++){
        
        if(text[i] >= 'A' && text[i] <= 'Z')
        printf("%s ", morseCodes[text[i] - 'A']);
        
        else if(text[i] >= 'a' && text[i] <= 'z')
        printf("%s ", morseCodes[text[i] - 'a']);
        
        else if(text[i] == ' ')
        printf(" / ");
        
        else printf(" ");
        
    }
    
}


void morse_to_text(const char* morse) {

    printf("\n");

    int morseLen = strlen(morse);
    char* text = (char*)malloc((morseLen / 4) + 1);

    int i = 0, textIndex = 0;
    while (i < morseLen) {
        if (morse[i] == ' ') {
            text[textIndex] = ' ';
            i++;
        } else {
            
            int codeLen = 0;
            while (i + codeLen < morseLen && morse[i + codeLen] != ' ') {
                codeLen++;
            }

            int letterIndex = -1;
            for (int j = 0; j < 26; j++) {
                if (codeLen == strlen(morseCodes[j]) && strncmp(&morse[i], morseCodes[j], codeLen) == 0) {
                    letterIndex = j;
                    break;
                }
            }

            
            if (letterIndex != -1) {
                text[textIndex] = letters[letterIndex];
                i += codeLen;
            } else {
                i++;
            }
        }
        textIndex++;
    }

    text[textIndex] = '\0'; 
    
    textIndex = 0;
    while(text[textIndex] != '\0'){
        printf("%c", text[textIndex]);
        textIndex++;
    }
}
