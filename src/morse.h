#ifndef MORSE_H
#define MORSE_H


void text_to_morse(const char* text);
void morse_to_text(const char* morse);


#endif
