#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "morse.h"

char* read_file(const char* filename){

    FILE* file = fopen(filename, "r");
    
    if(file == NULL)
    {
        printf("Error opening file: %s\n", filename);
        
        return NULL;
    }
    
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);
    
    char* text = (char*)malloc((file_size + 1) * sizeof(char));
    
    fread(text, sizeof(char), file_size, file);
    
    return text;
}

void real_time_translator(const char* option){
    if(option == "t")
    {
        char text[100];
        while(1){
            printf("Enter text to translate: ");
            fgets(text, sizeof(text), stdin);
            
            if (strlen(text) != 0)
                text_to_morse(text);
            else
                break;
        }
    }
    
    if(option == "m")
    {
        char text[100];
        while(1){
            printf("Enter morse code to decode: ");
            fgets(text, sizeof(text), stdin);
            
            if (strlen(text) != 0)
                text_to_morse(text);
            else
                break;
        }
    }
    
    if(option != "m" && option != "t")
    {
        printf("Error. Bad option. t - Text to morse. m - Morse to text");
    }
}

void print_logo(){
    printf(
        "|-----------------------------------|\n"
        "|-----------MORSE DECODER-----------|\n"
        "|-----------------------------------|\n"
    );
}

void print_text_menu(){
    print_logo();
    printf(
        "\n"
        "Select from menu:\n"
        "1. Translate morse from text file\n"
        "2. Translate text file to morse\n"
        "3. Real time morse to text translator. Enter key - exit\n"
        "4. Real time text to morse translator. Enter key - exit\n"
    );
}

int main(){
    
    char 
        choise, 
        file_name[100];
        
    print_text_menu();
    scanf("%c", &choise);
    switch(choise){
        case '1':
            printf("Enter file name: ");
            scanf("%s", file_name);
            if(read_file(file_name) != NULL){
                morse_to_text(read_file(file_name));
            }
            break;
        case '2':
            printf("Enter file name: ");
            scanf("%s", file_name);
            if(read_file(file_name) != NULL){
                text_to_morse(read_file(file_name));
            }
            break;
        case '3':
            printf("Selected: %c\n", choise);
            real_time_translator("m");
            break;
        case '4':
            printf("Selected: %c\n", choise);
            real_time_translator("t");
            break;
        default:
            printf("Incorrent selection");
            break;
    }
    
    return 0;
}
